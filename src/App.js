import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import React, {useState} from 'react';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import '../src/css/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ErrorPage from './components/ErrorPage';

export default function App() {
    const [loggedIn, setLoggedIn] = useState(false);
    const login = () => {
        setLoggedIn(true);
    };
    const logout = () => {
        setLoggedIn(false);
    };

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={<Login login={login}/>}/>
                <Route
                    path="/dashboard"
                    element={
                        <SecureRoute loggedIn={loggedIn}>
                            <Dashboard logout={logout}/>
                        </SecureRoute>
                    }
                />
                <Route path="*" element={<ErrorPage statusCode={404} errorMessage={"Not Found"}/>}/>
            </Routes>
        </BrowserRouter>
    );
}

function SecureRoute({loggedIn, children}) {
    return loggedIn ? children : <Navigate to="/login"/>;
}
