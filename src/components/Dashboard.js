import dayjs from 'dayjs';
import {useEffect, useState} from 'react';
import WeatherCard from './WeatherCard';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import '../css/Dashboard.css';

function fetchLocation(position, setLocation) {
    const url = `http://metaweather.com/api/location/search/?lattlong=${position.coords.latitude},${position.coords.longitude}`;
    const defaultLocation = {title: 'Kyiv', woeid: 924938};
    fetch(url, {mode: 'no-cors'})
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            data ? setLocation(data[0]) : setLocation(defaultLocation);
        })
        .catch((e) => {
            setLocation(defaultLocation);
        });
}

function ShowWeather({woeid}) {
    let weatherGroup = [];
    let date = dayjs();
    for (let i = 0; i < 7; i++) {
        weatherGroup.push(<WeatherCard key={i} date={date} woeid={woeid}/>);
        date = date.add(1, 'day');
    }

    return <div className="weather-placeholder">{weatherGroup}</div>;
}


const Dashboard = ({logout}) => {
    const [location, setLocation] = useState(null);

    useEffect(() => getGeoLocation(setLocation), []);

    let innerPage;
    if (!location) {
        innerPage = (
            <div className="spinner">
                <Spinner animation="border" role="status" variant="primary"/>
            </div>
        );
    } else {
        innerPage = (
            <main>
                <h1>{location.title}</h1>
                <ShowWeather woeid={location.woeid}/>
            </main>
        );
    }

    return (
        <div className="main-page">
            <nav className="nav-bar">
                <Button variant="outline-secondary" onClick={logout}>
                    Logout
                </Button>
            </nav>
            {innerPage}
        </div>
    );
};

function getGeoLocation(setLocation) {
    const geo = navigator.geolocation;
    geo.getCurrentPosition(
        (currentPosition) => {
            fetchLocation(currentPosition, setLocation);
        },
        () => {
            setLocation({title: 'Kyiv', woeid: 924938});
        }
    );
}

export default Dashboard;
