import React from 'react';
import {useLocation} from 'react-router-dom';
import '../ErrorPage/NotFound.css';
import Card from 'react-bootstrap/Card';

function ErrorPage({statusCode, errorMessage}) {
    return (
        <div>
            <Card className="error-card">
                <h1>{statusCode}</h1>
                <p>{errorMessage}</p>
                <code>{useLocation().pathname}</code>
            </Card>
        </div>
    );
}

export default ErrorPage;
