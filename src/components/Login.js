import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import {useNavigate} from "react-router-dom";
import {useState} from "react";
import '../css/login.css';

function handleSubmit(e, navigate, login, showError) {
    e.preventDefault();

    if (e.target.username.value !== 'JohnDoe') {
        showError('Wrong username!');
    } else if (e.target.password.value !== '1234') {
        showError('Wrong password!');
    } else if (e.target.username.value === 'JohnDoe'
        && e.target.password.value === '1234') {
        login();
        navigate('/dashboard');
    }
}

function Error({show, message}) {
    return (
        <Alert variant="danger" hidden={!show}>
            {message}
        </Alert>
    );
}

const Login = ({login}) => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [hasErrorMessage, setHasErrorMessage] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    const navigate = useNavigate();
    const hasError = (message) => {
        setHasErrorMessage(true);
        setErrorMessage(message);
    };

    function validateForm() {
        return username.length > 0 && password.length > 0;
    }

    return (
        <div>
            <Form className="login-form" onSubmit={(e) =>
                handleSubmit(e, navigate, login, hasError)}>
                <Form.Group>
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" name="username" placeholder="Username" value={username}
                                  onChange={(e) => setUsername(e.target.value)}
                                  required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name="password" placeholder="Password" value={password}
                                  onChange={(e) => setPassword(e.target.value)}
                                  required/>
                </Form.Group>
                <Button size="lg" type="submit" disabled={!validateForm()}>
                    Login
                </Button>
                <Error show={hasErrorMessage} message={errorMessage}/>
            </Form>
        </div>
    );
};

export default Login;
