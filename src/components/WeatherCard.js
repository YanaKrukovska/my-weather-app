import Spinner from 'react-bootstrap/Spinner';
import {useEffect, useState} from 'react';
import '../css/WeatherCard.css';
import Image from 'react-bootstrap/Image';
import Card from 'react-bootstrap/Card';
import 'dayjs/plugin/isToday';

function fetchWeather(date, woeid, setWeather) {
    const proxy = 'https://thingproxy.freeboard.io/fetch/';
    const baseUrl = 'http://metaweather.com/api/location';
    const url = `${proxy + baseUrl}/${woeid}/${date.format('YYYY/MM/DD')}`;
    fetch(url)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            setWeather(data[0]);
        });
}

const WeatherCard = ({date, woeid}) => {
    const [weatherData, setWeatherData] = useState(null);
    useEffect(() => fetchWeather(date, woeid, setWeatherData), []);
    if (!weatherData) {
        return (
            <div className="loading-card">
                <Spinner animation="border" variant="primary"/>{' '}
            </div>
        );
    } else if (weatherData) {
        const maxTemp = weatherData.max_temp;
        const minTemp = weatherData.min_temp;
        return (
            <div className="weather-card">
                <Card className="details">
                    <p className="date">
                        <b>{date.format('DD MMM')}</b>
                    </p>
                    <Image className="icon"
                           src={`https://www.metaweather.com/static/img/weather/${weatherData.weather_state_abbr}.svg`}
                           alt={weatherData.weather_state_abbr}
                    />
                    <p>Max: {maxTemp > 0 ? '+' + Math.round(maxTemp) : Math.round(maxTemp)}°C</p>
                    <p>Min: {minTemp > 0 ? '+' + Math.round(minTemp) : Math.round(minTemp)}°C</p>
                    <p>Wind: {Math.round(weatherData.wind_speed)}mph</p>
                    <p>Humidity: {Math.round(weatherData.humidity)}%</p>
                    <p>Pressure: {Math.round(weatherData.air_pressure)}mbar</p>
                </Card>
            </div>
        );
    }
};

export default WeatherCard;
